# README #

Add Maven dependency:

		<dependency>
			<groupId>com.example</groupId>
			<artifactId>ew-spring-boot-starter</artifactId>
			<version>1.0-SNAPSHOT</version>
		</dependency>

### What is this repository for? ###

* A Spring Boot starter for common configuration
* Version: 1.0-SNAPSHOT

### How do I get set up? ###

* Add to Maven dependencies. Spring Boot will automatically pick up the configuration.